<?php return array (
  'database' => 
  array (
    'default' => 
    array (
      'driver' => 'mysql',
      'host' => '192.168.99.100',
      'database' => 'mopas',
      'username' => 'root',
      'password' => 'toor',
      'port' => '32773',
      'charset' => 'utf8',
      'collation' => 'utf8_general_ci',
      'prefix' => '',
    ),
    'mopas' => 
    array (
      'driver' => 'mysql',
      'host' => '192.168.99.100',
      'database' => 'mopas',
      'username' => 'root',
      'password' => 'toor',
      'port' => '32773',
      'charset' => 'utf8',
      'collation' => 'utf8_general_ci',
      'prefix' => '',
    ),
  ),
  'view' => 
  array (
  ),
  'cache' => 
  array (
  ),
  'i18' => 
  array (
  ),
  'route' => 
  array (
    'root' => 
    array (
      'path' => '/',
      'action' => 'AppModule\\Controller\\Welcome::index',
      'params' => 
      array (
      ),
    ),
    'htmltest' => 
    array (
      'path' => '/test/html',
      'action' => 'AppModule\\Controller\\Welcome::htmlTest',
      'params' => 
      array (
      ),
    ),
    'jsontest' => 
    array (
      'path' => '/test/json',
      'action' => 'AppModule\\Controller\\Welcome::jsonTest',
      'params' => 
      array (
      ),
    ),
    'xmltest' => 
    array (
      'path' => '/test/xml',
      'action' => 'AppModule\\Controller\\Welcome::xmlTest',
      'params' => 
      array (
      ),
    ),
    'paramtest' => 
    array (
      'path' => '/test/param/{name}',
      'action' => 'AppModule\\Controller\\Welcome::paramTest',
      'params' => 
      array (
      ),
    ),
    'hashtest' => 
    array (
      'path' => '/test/hash/{hash}',
      'action' => 'AppModule\\Controller\\Welcome::hashTest',
      'params' => 
      array (
      ),
    ),
  ),
);