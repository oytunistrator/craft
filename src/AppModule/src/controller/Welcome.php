<?php
namespace AppModule\Controller;
use \Blue\DB as DB;
use \Blue\Message as Message;
use \Blue\Hash as Hash;

class Welcome extends DefaultController{
	function index(){
		return $this->phtml('index', [
			'version' => '1.0',
			'framework' => 'Blue Framework',
			'alert' => Message::success('<strong>Welcome!</strong>')
		]);
	}

	function htmlTest(){
		return $this->phtml('test', [
			'version' => '1.0',
			'framework' => 'Blue Framework',
			'alert' => Message::warning('This is <strong>test</strong> page.')
		]);
	}

	function paramTest($name){
		return $this->phtml('name', [
			'version' => '1.0',
			'framework' => 'Blue Framework',
			'name' => $name,
			'alert' => Message::info('Your param: <strong>'.$name.'</strong>')
		]);
	}

	function hashTest($hash){
		$encrypt = Hash::password($hash);
		return $this->phtml('hash', [
			'version' => '1.0',
			'framework' => 'Blue Framework',
			'password' => $hash,
			'key' => $encrypt,
			'alert' => Message::warning('Hash decrypted: <strong>'.$encrypt.'</strong>')
		]);
	}

	function jsonTest(){
		return $this->json(['result' => 'All systems online. Blue Framework JSON response is working good!']);
	}

	function xmlTest(){
		return $this->xml(['result' => 'All systems online. Blue Framework XML response is working good!']);
	}
}